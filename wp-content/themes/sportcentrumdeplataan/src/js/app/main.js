/**
 * Main
 *
 * The main file, page logic which is needed on all pages will be included here.
 * Please note for specific components like a slider create a separated components/<component_name>.js
 * The components will be loaded through the component loader and are not needed on every page!
 */

define( [ 'jquery', 'fastclick', 'mmenu' ], function( $, fast_click ) {
	$( function() {
		fast_click.attach( document.body );

		$( '#nav' ).mmenu(
			{
				scrollBugFix: true,
				offCanvas: {
					position: 'right'
				},
				navbar: {
					title: ''
				}
			}
		);

		$( 'a[href="#top"]' ).on(
			'click', function( event ) {
				event.preventDefault();

				$( 'html, body' ).animate( { scrollTop: 0 } );
			}
		);

		$('.header__main .is-toggle').click(function( event ) {
			event.preventDefault();
			$(this).toggleClass('is-active');
			$('.header__menu').fadeToggle('fast');
			$('.header__shape').toggleClass('header__shape--active');
		});
        if ( $(window).width() > 1024 ) {
            $('.timetable__table-res').hide();
            $('.timetable__title').addClass('toggle-down');

            $(document).on(
                'click', '.timetable__row-bg-pattern',
                function () {
                    var target = $(this).children('.timetable__table-res');
                    var targetTitle = $(this).find('.timetable__title');
                    if ($(targetTitle).hasClass('toggle-up')) {
                        $(targetTitle).removeClass('toggle-up');
                        $(targetTitle).addClass('toggle-down');
                    } else {
                        $(targetTitle).removeClass('toggle-down');
                        $(targetTitle).addClass('toggle-up');
                    }
                    $(target).slideToggle(500);
                }
            );
        }
            } );
} );
<?php
/**
 * The template part for displaying review content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$args = [
	'post_type'      => 'review',
	'posts_per_page' => - 1,
];

$review = new WP_Query( $args );

if ( $review->have_posts() ):?>
	<div class="reviews container">
		<div class="reviews__slide" data-component="carousel">
			<?php while ( $review->have_posts() ): $review->the_post(); ?>
				<div>
					<h3><?php the_title(); ?></h3>

					<?php the_content(); ?>
				</div>
			<?php endwhile;

			wp_reset_postdata(); ?>
		</div>
	</div>
<?php endif; ?>
<?php
/**
 * The template part for displaying schedule content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$opening_title = get_field( 'opening_title' );
$column_1      = get_field( 'column_1' );
$column_2      = get_field( 'column_2' );
$column_3      = get_field( 'column_3' );
$column_3      = get_field( 'column_3' );
$star_points   = get_field( 'star_points' );
$closing_title = get_field( 'closing_days_title' );
$holiday_title = get_field( 'holiday_title' ); ?>
<div class="schedule container">
	<div class="row">
		<div class="col-lg-8">
			<?php if ( ! empty( $opening_title ) ): ?>
				<h2><?php echo $opening_title; ?></h2>
			<?php endif;

			if ( have_rows( 'hours' ) ):?>
				<ul class="schedule__hours">
					<li class="row">
						<div class="col-sm-4 d-none d-sm-block"><h5><?php echo $column_1; ?></h5></div>
						<div class="col-sm-4 d-none d-sm-block"><h5><?php echo $column_2; ?></h5></div>
						<div class="col-sm-4 d-none d-sm-block"><h5><?php echo $column_3; ?></h5></div>
					</li>

					<?php while ( have_rows( 'hours' ) ): the_row(); ?>
						<li class="row">
							<div class="col-sm-4">
								<h5 class="d-block d-sm-none"><?php echo $column_1; ?></h5>

								<span><?php the_sub_field( 'day' ); ?></span>

								<?php the_sub_field( 'hours_1' ); ?>
							</div>
							<div class="col-sm-4">
								<h5 class="d-block d-sm-none"><?php echo $column_2; ?></h5>

								<span class="d-block d-sm-none"><?php the_sub_field( 'day' ); ?></span>

								<?php the_sub_field( 'hours_2' ); ?>
							</div>
							<div class="col-sm-4">
								<h5 class="d-block d-sm-none"><?php echo $column_3; ?></h5>

								<span class="d-block d-sm-none"><?php the_sub_field( 'day' ); ?></span>

								<?php the_sub_field( 'hours_3' ); ?>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>

			<?php if(($start = get_field('star_points')) && !empty($start)): ?>
				<em><?php echo $start; ?></em>
			<?php endif; ?>
		</div>

		<div class="col-lg-4">
			<?php if ( ! empty( $closing_title ) ): ?>
				<h2><?php echo $closing_title; ?></h2>
			<?php endif;

			if ( ! empty( $holiday_title ) ):?>
				<h5><?php echo $holiday_title; ?></h5>
			<?php endif;

			if ( have_rows( 'holidays' ) ):?>
				<ul class="schedule__holidays">
					<?php while ( have_rows( 'holidays' ) ): the_row(); ?>
						<li><?php the_sub_field( 'day' ); ?></li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
	</div>
</div>

